import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import javax.persistence.Converter

import com.fasterxml.jackson.databind.ObjectMapper
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import internal.GlobalVariable

import org.apache.poi.util.StringUtil
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import org.apache.commons.lang.StringUtils
import org.apache.poi.util.StringUtil




String sDownloadPathSystem = RunConfiguration.getProjectDir() + '/File Download'
String sDownloadPathKatalon = sDownloadPathSystem.replaceAll('/', '\\\\')
String link  = "\\File Download\\information.xlsx"
File file = new File(sDownloadPathKatalon+'/apis.txt')
JsonSlurper js = new JsonSlurper()
List<Map> m = js.parse(file)
	
	String s = m.fields.givenName.value
	givenName = s.replace("[", "").replace("]", "")

	String sdate = m.fields.dateOfBirth.value
	datofbirth = sdate.replace("[", "").replace("]", "")

	String ssex = m.fields.sex.value
	sex = ssex.replace("[", "").replace("]", "")
	
	String sage = m.fields.age.value
	age = sage.replace("[", "").replace("]", "")
	
	String saddress = m.fields.address.value
	address = saddress.replace("[", "").replace("]", "")


CustomKeywords.'common.excel.writeExcel'(link,'Name',0,1)
CustomKeywords.'common.excel.writeExcel'(link,'dateOfBirth',0,2)
CustomKeywords.'common.excel.writeExcel'(link,'Age',0,3)
CustomKeywords.'common.excel.writeExcel'(link,'Sex',0,4)
CustomKeywords.'common.excel.writeExcel'(link,'Address',0,5)
CustomKeywords.'common.excel.writeExcel'(link,givenName,1,1)
CustomKeywords.'common.excel.writeExcel'(link,datofbirth,1,2)
CustomKeywords.'common.excel.writeExcel'(link,age,1,3)
CustomKeywords.'common.excel.writeExcel'(link,sex,1,4)
CustomKeywords.'common.excel.writeExcel'(link,address,1,5)
















