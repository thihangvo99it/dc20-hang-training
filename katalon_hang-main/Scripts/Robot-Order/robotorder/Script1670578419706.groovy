import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.github.kklisura.cdt.protocol.types.page.Navigate
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.openqa.selenium.Rectangle as Rectangle
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.edge.EdgeDriver
import org.openqa.selenium.edge.EdgeOptions
import com.kms.katalon.core.configuration.RunConfiguration



browserName = DriverFactory.getExecutedBrowser().getName()

Date today = new Date()
String dateFolder = today.format('dd.MM.yyyy').replace('.', '-')
String nowTime = today.format('dd.MM.yyyy hh:mm:ss').replace(':', '.')
String sScreenshotPath = (((((RunConfiguration.getProjectDir() + '/ScreenShotImg')+ '/')+ dateFolder) + '/')+ nowTime) + '/'

'Setup download path'
String sDownloadPathSystem = RunConfiguration.getProjectDir() + '/File Download'

String sDownloadPathKatalon = sDownloadPathSystem.replaceAll('/', '\\\\')

Map sDownloadParameterDrive = [('download.default_directory') : sDownloadPathKatalon]

String FileCSV = "orders.csv"
'Check Excel file is Downloaded or not'
File download = new File(sDownloadPathSystem)
List name = Arrays.asList(download.list())
if(name.contains(FileCSV)) {
	
	'Delete File if exists'
	CustomKeywords.'common.deleteFile.DeleteDownloadFile'(sDownloadPathSystem, FileCSV)
	WebUI.delay(5)
}

'Change default download of browser'
if (browserName == 'EDGE_CHROMIUM_DRIVER') {
	Map edgeProfile = ["prefs": sDownloadParameterDrive]
	RunConfiguration.setWebDriverPreferencesProperty('ms:edgeOptions', edgeProfile)
}else if (browserName == 'CHROME_DRIVER'){
	RunConfiguration.setWebDriverPreferencesProperty('prefs', sDownloadParameterDrive)
}
'open browser'
WebUI.openBrowser('https://robocorp.com/docs/courses/build-a-robot')
WebDriver driver = DriverFactory.getWebDriver()
'maximize window'
driver.manage().window().maximize();
'downoad file '
driver.findElement(By.linkText('orders file')).click();
WebUI.delay(5)

'open browser'
WebUI.openBrowser('https://robotsparebinindustries.com/#/robot-order')
'maximize window'
WebUI.maximizeWindow()


'read file csv'
List<String> ListItem1= CustomKeywords.'common.getProfiles.getExcel'('File Download\\orders.csv')
println ListItem1
for(a=1;a<ListItem1.size();a++) {
	
	'click ok'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_ok'))

	'select head'
	WebUI.selectOptionByValue(findTestObject("Object Repository/Robot-order/select_head"),ListItem1[a].get(1), false)

	'input body'
	WebUI.click(findTestObject('Object Repository/Robot-order/div_body',[("value"):ListItem1[a].get(2)]))

	'input legs_number'
	WebUI.setText(findTestObject('Object Repository/Robot-order/input_legs',["value":"1"]), ListItem1[a].get(3))

	'input legs_address'
	WebUI.setText(findTestObject('Object Repository/Robot-order/input_legs',["value":"2"]), ListItem1[a].get(4))
	
	'click button proview'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_proview'))
	
	'click button orders'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_orders'))
	
	result = CustomKeywords.'common.verify.verifyObjectNotPresent'(findTestObject('Object Repository/Robot-order/div_message'))
	while(result==false) {
			
	'click button orders'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_orders'))
	
	result = CustomKeywords.'common.verify.verifyObjectNotPresent'(findTestObject('Object Repository/Robot-order/div_message'))
	}

	WebUI.takeElementScreenshot(((((sScreenshotPath + a) + '_')+ "Head"+ ListItem1[a].get(1)+"Body"+ListItem1[a].get(2)+"Legs"+ListItem1[a].get(3)+ "Address"+ListItem1[a].get(4))+'.png'),findTestObject('Object Repository/Robot-order/img_robotorder'))

	'click button Order another robot'
	WebUI.click(findTestObject('Object Repository/Robot-order/btn_order-another'))

}
WebUI.closeBrowser()

