package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.apache.commons.collections4.map.HashedMap
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.util.WorkbookUtil
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFCell
import org.apache.poi.xssf.usermodel.XSSFRow
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable


public class excel {

	@Keyword
	public readExcel(String link, int row, int cell) {
		String xpath = RunConfiguration.getProjectDir()
		FileInputStream excel = new FileInputStream(xpath+link)
		XSSFWorkbook workbook = new XSSFWorkbook(excel)
		XSSFSheet sheet = workbook.getSheet("Sheet1")
		Row Row = sheet.getRow(row)
		Cell cell1 = Row.getCell(cell)
		return cell1
		FileOutputStream fos = new FileOutputStream(xpath+link);
		workbook.write(fos);
		fos.close();
	}
	
	@Keyword
	public void writeExcel(String link, String text, int rownum, int colnum) throws Exception {
		Row row
		Cell cell
		String xpath = RunConfiguration.getProjectDir()
		FileInputStream excel = new FileInputStream(xpath+link)
		XSSFWorkbook workbook = new XSSFWorkbook(excel)
		XSSFSheet sheet = workbook.getSheet("Sheet1")
		try{
			row  = sheet.getRow(rownum);
			if(row ==null)
			{
				row = sheet.createRow(rownum);
			}
			cell = row.getCell(colnum);

			if (cell == null) {
				cell = row.createCell(colnum);
			}
			cell.setCellValue(text);
			FileOutputStream fos = new FileOutputStream(xpath+link);
			workbook.write(fos);
			fos.close();
	
				}catch(Exception e){
			throw (e);
		}

	}
	
	
	
}
