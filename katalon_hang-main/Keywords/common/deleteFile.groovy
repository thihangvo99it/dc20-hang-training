package common

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.util.KeywordUtil
import internal.GlobalVariable

public class deleteFile {

	@Keyword
	public boolean DeleteDownloadFile(String sDownloadPath, String sFileName) {

		File dir = new File(sDownloadPath);
		File[] dirContents = dir.listFiles();
		String lastAttempt = "";

		if (dirContents.length > 0) {
			for (int i = 0; i < dirContents.length; i++) {
				if (dirContents[i].getName() == sFileName) {
					// File has been found, it can now be deleted:
					dirContents[i].delete();
					KeywordUtil.markPassed('File exist in ' + sDownloadPath + ' and was deleted')
					return true
				}
				lastAttempt = dirContents[i].getName() == sFileName;
			}
			if (lastAttempt != sFileName) {
				KeywordUtil.markFailed('File does not exist in ' + sDownloadPath)
				return false
			}
		}
		KeywordUtil.markFailed('File does not exist in ' + sDownloadPath)
		return false;
	}
}
