<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>add</name>
   <tag></tag>
   <elementGuidId>30a2971a-4641-4ceb-994e-4430b724f65f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//div[@class=&quot;product-item&quot;] //button[@class=&quot;button-2 product-box-add-to-cart-button&quot;])[${rnd}]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
