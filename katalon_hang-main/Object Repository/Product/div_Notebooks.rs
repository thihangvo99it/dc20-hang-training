<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Notebooks</name>
   <tag></tag>
   <elementGuidId>e9ddb872-4107-4cee-b99e-125a8a186078</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[normalize-space()='Notebooks']//parent::div//following-sibling::div//div//div[@data-productid=&quot;${value}&quot;]//button</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
