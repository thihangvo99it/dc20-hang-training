<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_message</name>
   <tag></tag>
   <elementGuidId>a318193d-acdd-4723-b94b-42a07c8fc1f3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//p[contains(text(),'The product has been added to your')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
