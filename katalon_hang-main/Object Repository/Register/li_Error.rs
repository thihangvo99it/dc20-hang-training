<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Error</name>
   <tag></tag>
   <elementGuidId>9071858b-0458-4e11-9e9c-9cacf6bf7437</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[contains(.,&quot;The specified email already exists&quot;)]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
